<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupos_model extends CI_Model {

	public function getGrupos(){
		$this->db->where("estado","1");
		$resultados = $this->db->get("grupos");
		return $resultados->result();
	}

	public function grilla($starIndex, $pageSize, $buscar){
		$cont=$this->db->count_all_results('grupos'); 
		$this->db->select("*");
		$this->db->from("grupos");
		$this->db->where("(id LIKE '%$buscar%' OR nombre LIKE '%$buscar%') AND estado='1'");
		$this->db->limit($pageSize);
		$this->db->offset($starIndex);
		$this->db->order_by('id', 'DESC');
		return [$this->db->get()->result_array(), $cont];
	} 


	public function save($data){ /** guarda los GrupoS */
		return $this->db->insert("grupos",$data);
	}

	public function getGrupo($id){ /** para la opcion editar **/
		$this->db->where("id",$id);
		$resultado = $this->db->get("grupos");
		return $resultado->row();

	}

	public function update($id,$data){ /** actualiza los datos **/
		$this->db->where("id",$id);
		return $this->db->update("grupos",$data);
	}

}
