<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cursos_model extends CI_Model {

	public function getCursos(){
		$this->db->select("c.*,c.id,c.nombre, c.tipocurso_id, tc.descripcion, c.costo");
		$this->db->from("cursos as c");
		$this->db->join("tipo_curso as tc", "c.tipocurso_id = tc.id");
		$this->db->where("c.estado", "1");
		$resultados = $this->db->get();
		if ($resultados->num_rows() > 0) {
			return $resultados->result();
		} else {
			return false;
		}
	}

	public function grilla($starIndex, $pageSize, $buscar){
		$cont=$this->db->count_all_results('cursos'); 
		$this->db->select("c.id,c.nombre,tc.descripcion,c.costo");
		$this->db->from("cursos as c");
		$this->db->join("tipo_curso tc","c.tipocurso_id = tc.id");
		$this->db->or_where("(c.nombre LIKE '%$buscar%' OR tc.descripcion LIKE '%$buscar%') AND c.estado=1");
		$this->db->limit($pageSize);
		$this->db->offset($starIndex);
		$this->db->order_by('c.id', 'DESC');
		return [$this->db->get()->result_array(), $cont];
	} 




	public function getTipoCursos(){
		$resultados = $this->db->get("tipo_curso");
		return $resultados->result();
	}
	public function save($data){ /** guarda los Cursos */
		return $this->db->insert("cursos",$data);
	}

	public function getCurso($id){ /** para la opcion editar **/
		$this->db->where("id",$id);
		$resultado = $this->db->get("cursos");
		return $resultado->row();

	}

	public function update($id,$data){ /** actualiza los datos **/
		$this->db->where("id",$id);
		return $this->db->update("cursos",$data);
	}

}
