<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modulos_model extends CI_Model {

	public function getModulos(){
		$this->db->select("m.*,c.id as curso_id, m.id,c.nombre as curso,m.nombre,m.abreviatura,m.hora");
		$this->db->from("modulos m");
		$this->db->join("cursos c","m.curso_id = c.id");
		$this->db->where("m.estado","1");
		//$this->db->join("tipo_comprobante tc","v.tipo_comprobante_id = tc.id");
		$resultados = $this->db->get();
		if ($resultados->num_rows() > 0) {
			return $resultados->result();
		}else
		{
			return false;
		}
	}

	public function grilla($starIndex, $pageSize, $buscar){
		$cont=$this->db->count_all_results('modulos'); 
		$this->db->select("c.id,m.id as idm,c.nombre as curso, m.nombre AS modulo,m.hora");
		$this->db->from("modulos as m");
		$this->db->join("cursos as c","c.id = m.curso_id");
		$this->db->or_where("(c.nombre LIKE '%$buscar%' OR m.nombre LIKE '%$buscar%') AND m.estado=1");
		$this->db->limit($pageSize);
		$this->db->offset($starIndex);
		$this->db->order_by('m.id', 'DESC');
		return [$this->db->get()->result_array(), $cont];
	} 


	public function getModulo($id){ /** guarda los Modulos */
		$this->db->select("m.*,m.id,m.nombre,m.abreviatura,m.hora");
		$this->db->from("modulos m");
		$this->db->join("cursos c","m.curso_id = c.id");
		$this->db->where("m.estado","1");
		$this->db->where("c.id",$id);
		//$this->db->join("tipo_comprobante tc","v.tipo_comprobante_id = tc.id");
		$resultados = $this->db->get();
		if ($resultados->num_rows() > 0) {
			return $resultados->result();
		}else
		{
			return false;
		}
	}

	public function save($data){ /** guarda los Modulos */
		return $this->db->insert("modulos",$data);
	}

	public function getCurso($id){ /** para la opcion editar **/
		$this->db->select("c.*,c.id,c.nombre, tc.descripcion");
		$this->db->from("cursos as c");
		$this->db->join("tipo_curso as tc", "c.tipocurso_id = tc.id");
		$this->db->where("c.id", $id);
		$resultado = $this->db->get();
		return $resultado->row();
	}

	public function update($id,$data){ /** actualiza los datos **/
		$this->db->where("id",$id);
		return $this->db->update("modulos",$data);
	}




}
