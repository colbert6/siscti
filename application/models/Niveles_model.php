<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Niveles_model extends CI_Model {

	public function getNiveles(){
		$this->db->where("estado","1");
		$resultados = $this->db->get("niveles");
		return $resultados->result();
	}

	public function grilla($starIndex, $pageSize, $buscar){
		$cont=$this->db->count_all_results('niveles'); 
		$this->db->select("*");
		$this->db->from("niveles");
		$this->db->where("(id LIKE '%$buscar%' OR nombre LIKE '%$buscar%') AND estado='1'");
		$this->db->limit($pageSize);
		$this->db->offset($starIndex);
		$this->db->order_by('id', 'DESC');
		return [$this->db->get()->result_array(), $cont];
	} 


	public function save($data){ /** guarda los Niveles */
		return $this->db->insert("niveles",$data);
	}

	public function getNivel($id){ /** para la opcion editar **/
		$this->db->where("id",$id);
		$resultado = $this->db->get("niveles");
		return $resultado->row();

	}

	public function update($id,$data){ /** actualiza los datos **/
		$this->db->where("id",$id);
		return $this->db->update("niveles",$data);
	}

}
