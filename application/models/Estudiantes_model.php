<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estudiantes_model extends CI_Model {

	public function getEstudiantes(){
		$this->db->where("estado","1");
		$resultados = $this->db->get("estudiantes");
		return $resultados->result();
	}

	public function grilla($starIndex, $pageSize, $buscar){
		$cont=$this->db->count_all_results('estudiantes'); 
		$this->db->select("e.id, e.num_documento,e.nombre,e.celular, e.fecha_registro");
		$this->db->from("estudiantes as e");
		$this->db->join("sexo s","e.sexo_id = s.id");
		$this->db->or_where("(e.num_documento LIKE '%$buscar%' OR e.nombre LIKE '%$buscar%') AND e.estado=1");
		$this->db->limit($pageSize);
		$this->db->offset($starIndex);
		$this->db->order_by('e.id', 'DESC');
		return [$this->db->get()->result_array(), $cont];
	} 

	public function one($id){
		$this->db->select('*');
		$this->db->from('estudiantes');
		$this->db->where('id',$id);
		return $this->db->get()->row_array();

	}
	public function create($params){
		$this->db->insert('estudiantes',$params);
		return $this->bd->insert();
	}



	public function save($data){ /** guarda los estudiantes */
		return $this->db->insert("estudiantes",$data);
	}

	public function getEstudiante($id){ /** para la opcion editar **/
		$this->db->where("id",$id);
		$resultado = $this->db->get("estudiantes");
		return $resultado->row();

	}

	public function update($id,$data){ /** actualiza los datos **/
		$this->db->where("id",$id);
		return $this->db->update("estudiantes",$data);
	}


	public function getVer($id){ /** para cargar la informacion en el boton ver **/

		$this->db->select("e.*,e.nombre,s.nombre as sexo,e.fecha_nacimiento,td.nombre as tipodocumento,e.num_documento,e.direccion,e.telefono,e.celular,e.email,c.nombre as carrera");	
		$this->db->from("estudiantes e");
		$this->db->join("sexo s","e.sexo_id = s.id");
		$this->db->join("tipo_documento td","e.tipo_documento_id = td.id");
		$this->db->join("carreras c","e.carrera_id = c.id");
		$this->db->where("e.id",$id);
		$resultado = $this->db->get();
		return $resultado->row();
	}



/** cargar combos en add */

	public function getCarreras(){
		$resultados = $this->db->get("carreras");
		return $resultados->result();
	}

	public function getTipoDocumentos(){
		$resultados = $this->db->get("tipo_documento");
		return $resultados->result();
	}

		public function getSexos(){
		$resultados = $this->db->get("sexo");
		return $resultados->result();
	}


}
