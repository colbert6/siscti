<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aulas_model extends CI_Model {

	public function getAulas(){
		$this->db->where("estado","1");
		$resultados = $this->db->get("aulas");
		return $resultados->result();
	}

	public function grilla($starIndex, $pageSize, $buscar){
		$cont=$this->db->count_all_results('aulas'); 
		$this->db->select("*");
		$this->db->from("aulas");
		$this->db->where("(id LIKE '%$buscar%' OR nombre LIKE '%$buscar%') AND estado='1'");
		$this->db->limit($pageSize);
		$this->db->offset($starIndex);
		$this->db->order_by('id', 'DESC');
		return [$this->db->get()->result_array(), $cont];
	} 



	public function save($data){ /** guarda los aulas */
		return $this->db->insert("aulas",$data);
	}

	public function getAula($id){ /** para la opcion editar **/
		$this->db->where("id",$id);
		$resultado = $this->db->get("aulas");
		return $resultado->row();

	}

	public function update($id,$data){ /** actualiza los datos **/
		$this->db->where("id",$id);
		return $this->db->update("aulas",$data);
	}

}
