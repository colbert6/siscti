<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Docentes_model extends CI_Model {

	public function getDocentes(){
		$this->db->where("estado","1");
		$resultados = $this->db->get("docentes");
		return $resultados->result();
	}
	public function grilla($starIndex, $pageSize, $buscar){
		$cont=$this->db->count_all_results('docentes'); 
		$this->db->select("e.id, e.num_documento,e.nombre,e.celular, e.fecha_registro");
		$this->db->from("docentes as e");
		$this->db->join("sexo s","e.sexo_id = s.id");
		$this->db->or_where("(e.num_documento LIKE '%$buscar%' OR e.nombre LIKE '%$buscar%') AND e.estado=1");
		$this->db->limit($pageSize);
		$this->db->offset($starIndex);
		$this->db->order_by('e.id', 'DESC');
		return [$this->db->get()->result_array(), $cont];
	} 




	public function save($data){ /** guarda los Docentes */
		return $this->db->insert("docentes",$data);
	}

	public function getDocente($id){ /** para la opcion editar **/
		$this->db->where("id",$id);
		$resultado = $this->db->get("docentes");
		return $resultado->row();

	}

	public function update($id,$data){ /** actualiza los datos **/
		$this->db->where("id",$id);
		return $this->db->update("docentes",$data);
	}


	public function getVer($id){ /** para cargar la informacion en el boton ver **/

		$this->db->select("d.*,d.nombre,s.nombre as sexo,d.fecha_nacimiento,td.nombre as tipodocumento,d.num_documento,d.direccion,d.telefono,d.celular,d.email,c.nombre as carrera");	
		$this->db->from("docentes d");
		$this->db->join("sexo s","d.sexo_id = s.id");
		$this->db->join("tipo_documento td","d.tipo_documento_id = td.id");
		$this->db->join("carreras c","d.carrera_id = c.id");
		$this->db->where("d.id",$id);
		$resultado = $this->db->get();
		return $resultado->row();
	}



/** cargar combos en add */

	public function getCarreras(){
		$resultados = $this->db->get("carreras");
		return $resultados->result();
	}

	public function getTipoDocumentos(){
		$resultados = $this->db->get("tipo_documento");
		return $resultados->result();
	}

		public function getSexos(){
		$resultados = $this->db->get("sexo");
		return $resultados->result();
	}


}
