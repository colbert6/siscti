<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			ESTUDIANTES
			<small>Listado</small>
		</h1>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- Default box -->
		<div class="box box-solid">
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<?php if ($permisos->insert == 1) : ?>
							<!-- para permisos  -->
							<a href="<?php echo base_url(); ?>mantenimiento/estudiantes/add" class="btn btn-primary btn-flat"><span class="fa fa-plus"></span> Agregar Estudiante</a>
						<?php endif; ?>
					</div>
				</div>
				<hr>
			
			<div class="row">
					<div class="table-responsive col-md-12">
						<table id="example1" >
							<thead>
								<tr>
									<th>DNI / RUC</th>
									<th>ESTUDIANTE</th>
									<th>TELÉFONO</th>
									<th>CELULAR</th>
									<th>OPCIÓN</th>
								</tr>
							</thead>
							<tbody>
								<?php if (!empty($estudiantes)) : ?>
									<?php foreach ($estudiantes as $estudiante) : ?>
										<tr>
											<td><?php echo $estudiante->num_documento; ?></td>
											<td><?php echo $estudiante->nombre; ?></td>
											<td><?php echo $estudiante->telefono; ?></td>
											<td><?php echo $estudiante->celular; ?></td>
											<td>
												<div class="btn-group">
													<button type="button" class="btn btn-info btn-view-estudiantes" data-toggle="modal" data-target="#modal-estudiantes" value="<?php echo $estudiante->id; ?>">
														<span class="fa fa-search"></span>
													</button>
													<?php if ($permisos->update == 1) : ?>
														<!-- para permisos  -->
														<a href="<?php echo base_url() ?>mantenimiento/estudiantes/edit/<?php echo $estudiante->id; ?>" class="btn btn-warning"><span class="fa fa-pencil"></span></a>
													<?php endif; ?>
													<?php if ($permisos->delete == 1) : ?>
														<!-- para permisos  -->
														<a href="<?php echo base_url(); ?>mantenimiento/estudiantes/delete/<?php echo $estudiante->id; ?>" class="btn btn-danger btn-remove"><span class="fa fa-remove"></span></a>
													<?php endif; ?>
												</div>
											</td>
										</tr>
									<?php endforeach; ?>
								<?php endif; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="modal-estudiantes">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">INFORMACIÓN DEL ESTUDIANTE</h4>
			</div>
			<div class="modal-body">

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
