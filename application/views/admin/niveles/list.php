<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        NIVELES
        <small>Listado</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php if($permisos->insert == 1):?>    <!-- para permisos  -->
                        <a href="<?php echo base_url();?>mantenimiento/niveles/add" class="btn btn-primary btn-flat"><span class="fa fa-plus"></span> Agregar Nivel</a>
                    <?php endif;?>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="table-responsive col-md-12">
                        <table id="example1" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>NOMBRE</th>
                                    <th>DESCUENTO</th>
                                    <th>OPCIÓN</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!empty($niveles)):?>
                                    <?php foreach($niveles as $nivel):?>
                                        <tr>
                                            <td><?php echo $nivel->id;?></td>
                                            <td><?php echo $nivel->nombre;?></td>
                                            <td><?php echo ($nivel->descuento)*100;?>%</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info btn-view-niveles" data-toggle="modal" data-target="#modal-niveles" value="<?php echo $nivel->id;?>">
                                                        <span class="fa fa-search"></span>
                                                    </button>
                                                    <?php if($permisos->update == 1):?>  <!-- para permisos  -->
                                                    <a href="<?php echo base_url()?>mantenimiento/niveles/edit/<?php echo $nivel->id;?>" class="btn btn-warning"><span class="fa fa-pencil"></span></a>
                                                <?php endif;?>
                                                <?php if($permisos->delete == 1):?>  <!-- para permisos  -->
                                                    <a href="<?php echo base_url();?>mantenimiento/niveles/delete/<?php echo $nivel->id;?>" class="btn btn-danger btn-remove"><span class="fa fa-remove"></span></a>
                                                <?php endif;?>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="modal-niveles">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">INFORMACIÓN DEL NIVEL</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
