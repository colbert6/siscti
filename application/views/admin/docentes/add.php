<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Docentes
        <small>Nuevo</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php if($this->session->flashdata("error")):?>
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error"); ?></p>
                             </div>
                        <?php endif;?>
                        <form action="<?php echo base_url();?>mantenimiento/Docentes/store" method="POST">
							<div class="form-group <?php echo form_error("tipodocumento") != false ? 'has-error':'';?>">
                                <label for="tipodocumento">Tipo de Documento</label>
                                <select name="tipodocumento" id="tipodocumento" class="form-control" >
                                    <option value="">Seleccione...</option>
                                    <?php foreach ($tipodocumentos as $tipodocumento) :?>
                                        <option value="<?php echo $tipodocumento->id;?>" <?php echo set_select("tipodocumento",$tipodocumento->id);?>><?php echo $tipodocumento->nombre ?></option>
                                    <?php endforeach;?>
                                </select>
                                <?php echo form_error("tipodocumento","<span class='help-block'>","</span>");?>
                            </div>

                            <div class="form-group <?php echo form_error("numero") != false ? 'has-error':'';?>">
                                <label for="numero">Numero del Documento:</label>
                                <input type="text" class="form-control" id="numero" name="numero" value="<?php echo set_value("numero");?>">
                                <button id="btn-consultar-dni" type="button" class="btn btn-primary  "><span class="fa fa-search"></span> Consultar Documento  </button>
                                <?php echo form_error("numero","<span class='help-block'>","</span>");?>
                            </div>
                            <div class="form-group <?php echo form_error('nombre') == true ? 'has-error':''?>">
                                <label for="nombre">Apellidos y Nombres:</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo set_value("nombre");?>">
                                <?php echo form_error("nombre","<span class='help-block'>","</span>");?>
                            </div>


                            <div class="form-group <?php echo form_error("sexo") != false ? 'has-error':'';?>">
                                <label for="sexo">Sexo:</label>
                                <select name="sexo" id="sexo" class="form-control" >
                                    <option value="">Seleccione...</option>
                                    <?php foreach ($sexos as $sexo) :?>
                                        <option value="<?php echo $sexo->id;?>" <?php echo set_select("sexo",$sexo->id);?>><?php echo $sexo->nombre ?></option>
                                    <?php endforeach;?>
                                </select>
                                <?php echo form_error("sexo","<span class='help-block'>","</span>");?>
                            </div>

                            <div class="form-group <?php echo form_error('fecha_naci') == true ? 'has-error':''?>">
                                <label for="fecha_naci">Fecha de Nacimiento:</label>
                                <input type="date" class="form-control" id="fecha_naci" name="fecha_naci" value="<?php echo set_value("fecha_naci");?>">
                                <?php echo form_error("fecha_naci","<span class='help-block'>","</span>");?>
                            </div>

                            <div class="form-group <?php echo form_error("direccion") != false ? 'has-error':'';?>">
                                <label for="direccion">Direccion:</label>
                                <input type="text" class="form-control" id="direccion" name="direccion" value="<?php echo set_value("direccion");?>">
                                <?php echo form_error("direccion","<span class='help-block'>","</span>");?>
                            </div>
                           
                           
                            <div class="form-group <?php echo form_error('telefono') == true ? 'has-error':''?>">
                                <label for="telefono">Telefono:</label>
                                <input type="text" class="form-control" id="telefono" name="telefono">
                                <?php echo form_error("telefono","<span class='help-block'>","</span>");?>
                            </div>

                            <div class="form-group <?php echo form_error('celular') == true ? 'has-error':''?>" >
                                <label for="celular">Celular:</label>
                                <input type="text" class="form-control" id="celular" name="celular" value="<?php echo set_value("celular");?>" >
                                <?php echo form_error("celular","<span class='help-block'>","</span>");?>
                            </div>
                            <div class="form-group <?php echo form_error('email') == true ? 'has-error':''?>" >
                                <label for="email">Correo Electronico:</label>
                                <input type="text" class="form-control" id="email" name="email" value="<?php echo set_value("email");?>" >
                                <?php echo form_error("email","<span class='help-block'>","</span>");?>
                            </div>

                            <div class="form-group <?php echo form_error("carrera") != false ? 'has-error':'';?>">
                                <label for="carrera">Carrera / Especialidad </label>
                                <select name="carrera" id="carrera" class="form-control" >
                                    <option value="">Seleccione...</option>
                                    <?php foreach ($carreras as $carrera) :?>
                                        <option value="<?php echo $carrera->id;?>" <?php echo set_select("carrera",$carrera->id);?>><?php echo $carrera->nombre ?></option>
                                    <?php endforeach;?>
                                </select>
                                <?php echo form_error("carrera","<span class='help-block'>","</span>");?>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-flat">Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
