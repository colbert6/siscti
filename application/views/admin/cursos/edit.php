
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        CURSO / EVENTO
        <small>Editar</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php if($this->session->flashdata("error")):?>
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error"); ?></p>
                             </div>
                        <?php endif;?>
                        <form action="<?php echo base_url();?>mantenimiento/Cursos/update" method="POST" enctype="multipart/form-data" id="myForm">
                             <input type="hidden" name="idcurso" value="<?php echo $curso->id;?>">
                            <div class="form-group <?php echo form_error('nombre') == true ? 'has-error':''?>">
                                <label for="nombre">NOMBRE:</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo form_error("nombre") !=false ? set_value("nombre") : $curso->nombre;?>">
                                <?php echo form_error("nombre","<span class='help-block'>","</span>");?>
                            </div>

                            <div class="form-group <?php echo form_error('silabo') == true ? 'has-error':''?>">
                                <label for="silabo">SILABOS : <?php echo $curso->silabo;?></label>
                                 <input type="hidden" name="silabos" value="<?php echo $curso->silabo;?>">
                                <input type="file" class="form-control" id="silabo" name="silabo" value="<?php echo form_error("silabo") !=false ? set_value("silabo") : $curso->silabo;?>"> 
                                <?php echo form_error("silabo","<span class='help-block'>","</span>");?>
                            </div>

                            <div class="form-group <?php echo form_error('costo') == true ? 'has-error':''?>">
                                <label for="costo">PRECIO:</label>
                                <input type="text" class="form-control" id="costo" name="costo" value="<?php echo form_error("costo") !=false ? set_value("costo") : $curso->costo;?>">
                                <?php echo form_error("costo","<span class='help-block'>","</span>");?>
                            </div>
                            <div class="form-group <?php echo form_error("tipocurso") != false ? 'has-error':'';?>">
                               
                                    <label for="tipocurso">TIPO CURSO</label>
                                    <select name="tipocurso" id="tipocurso" class="form-control" >
                                        <option value="">Seleccione...</option>
                                        <?php if(form_error("tipocurso")!=false || set_value("tipocurso") != false): ?>
                                            <?php foreach ($tipocursos as $tipocurso) :?>
                                                <option value="<?php echo $tipocurso->id;?>" <?php echo set_select("tipocurso",$tipocurso->id);?>><?php echo $tipocurso->descripcion ?></option>
                                            <?php endforeach;?>
                                        <?php else: ?>
                                            <?php foreach ($tipocursos as $tipocurso) :?>
                                                <option value="<?php echo $tipocurso->id;?>" <?php echo $tipocurso->id == $curso->tipocurso_id? 'selected':'';?>><?php echo $tipocurso->descripcion ?></option>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    </select>
                  
                                <?php echo form_error("tipocurso","<span class='help-block'>","</span>");?>
                            </div>

                            <div class="form-group <?php echo form_error('inscripcion_web') == true ? 'has-error':''?>">
                                <label for="estado">Inscripcion web : </label>
                                <select  class="form-control" id="inscripcion_web" name="inscripcion_web">
                                    <option <?= $curso->inscripcion_web=='mostrar'? 'selected':'' ?> value="mostrar">Mostrar</option>
                                    <option <?= $curso->inscripcion_web=='no mostrar'? 'selected':'' ?> value="no mostrar">No mostrar</option>
                                </select>                               
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-flat">Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
