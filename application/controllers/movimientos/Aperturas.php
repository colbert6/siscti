<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Aperturas extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata("login")) {
			redirect(base_url());
		}
		$this->permisos = $this->backend_lib->control();/* crear para permisos de modulos  */
		$this->load->model("Aperturas_model");
		$this->load->model("Cursos_model");
		$this->load->model("Grupos_model");
	}

	public function index()
	{

		$datouno = $this->input->post("datouno");
		$datodos = $this->input->post("datodos");

		if ($this->input->post("buscar")) {
			$cursospre = $this->Aperturas_model->getCursospre($datouno, $datodos);
		} else {
			$cursospre = $this->Aperturas_model->getAperturas();
		}

		$data  = array(
			'permisos' => $this->permisos, /* crear para permisos de modulos  */
			'Aperturas' => $cursospre,
			'curgrupres' => $this->Aperturas_model->getCurgrupres(),

		);
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/aperturas/listjt", $data);
		$this->load->view("layouts/footer");
		$this->load->view("content/c_aperturas");
	}
	public function lista()
	{
		$starIndex = $_GET['jtStartIndex'];
		$pageSize = $_GET['jtPageSize'];
		$buscar = (isset($_POST['search']) ? $_POST['search']: '' );
		$libro = $this->Aperturas_model->grilla($starIndex, $pageSize, $buscar);
		$jTableResult['Result'] = 'OK';
		$jTableResult['Records'] = $libro[0];
		$jTableResult['TotalRecordCount'] = $libro[1];
		header('Content-Type: application/json');
		echo json_encode($jTableResult);
	}

	public function add()
	{
		$data = array(
			// "tipocomprobantes" => $this->Aperturas_model->getComprobantes(),
			"cursos" => $this->Cursos_model->getCursos(),
			//"estudiantes" => $this->Estudiantes_model->getestudiantes(),
			"grupos" => $this->Grupos_model->getGrupos(),
			//"niveles" => $this->Niveles_model->getNiveles(),
			"sedes" => $this->Aperturas_model->getSedes()
		);
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/aperturas/add", $data);
		$this->load->view("layouts/footer");
	}


	public function store()
	{
		$idusuario = $this->session->userdata("id");
		$idcurso = $this->input->post("idcurso");
		$idgrupo = $this->input->post("idgrupo");
		$fecha_ini = $this->input->post("fecha_ini");
		$sede_id = $this->input->post("sede_id");
		$estado_inscripcion = $this->input->post("estado_inscripcion");
		$this->form_validation->set_rules("curso", "Curso de la apertura", "required");
		$this->form_validation->set_rules("grupo", "Gupo de la apertura", "required");
		if ($this->form_validation->run() == TRUE) {

			$data  = array(
				'usuario_id' => $idusuario,
				'curso_id' => $idcurso,
				'grupo_id' => $idgrupo,
				'fecha_registro' => date('Y-m-d'),
				'fecha_ini' => $fecha_ini,
				'estado_inscripcion' => $estado_inscripcion,
				'notas' => "0",
				'estado' => "1",
				'sede_id' => $sede_id
			);
			if ($this->Aperturas_model->save($data)) {
				redirect(base_url() . "movimientos/aperturas");
			} else {
				$this->session->set_flashdata("error", "No se pudo guardar la informacion");
				redirect(base_url() . "movimientos/aperturas/add");
			}
		} else {
			/*redirect(base_url()."mantenimiento/Niveles/add");*/
			$this->add();
		}
	}
	public function edit($id)
	{
		$data  = array(
			"cursos" => $this->Cursos_model->getCursos($id),
			//"estudiantes" => $this->Estudiantes_model->getestudiantes($id),
			"grupos" => $this->Grupos_model->getGrupos($id),
			//"niveles" => $this->Niveles_model->getNiveles($id),
			'apertura' => $this->Aperturas_model->getApertura($id),
			"sedes" => $this->Aperturas_model->getSedes()
		);
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/aperturas/edit", $data);
		$this->load->view("layouts/footer");
	}

	public function update()
	{
		$idapertura = $this->input->post("idapertura");
		$idcurso = $this->input->post("idcurso");
		$idgrupo = $this->input->post("idgrupo");
		$fecha_ini = $this->input->post("fecha_ini");
		$sede_id = $this->input->post("sede_id");
		$estado_inscripcion = $this->input->post("estado_inscripcion");
		$this->form_validation->set_rules("curso", "Curso de la apertura", "required");
		$this->form_validation->set_rules("grupo", "Gupo de la apertura", "required");
		if ($this->form_validation->run() == TRUE) {

			$data  = array(
				'curso_id' => $idcurso,
				'grupo_id' => $idgrupo,
				'fecha_ini' => $fecha_ini,
				'estado_inscripcion' => $estado_inscripcion,
				'sede_id' => $sede_id
			);
			if ($this->Aperturas_model->update($idapertura, $data)) {
				redirect(base_url() . "movimientos/aperturas");
			} else {
				$this->session->set_flashdata("error", "No se pudo Actualizar la informacion");
				redirect(base_url() . "movimientos/aperturas/edit/" . $idapertura);
			}
		} else {
			/*redirect(base_url()."mantenimiento/Niveles/add");*/
			$this->edit($idapertura);
		}
	}


	public function view($id)
	{
		// echo $this->upload->display_errors();die;
		$data  = array(
			'apertura' => $this->Aperturas_model->getPrever($id),

		);
		// exit($data);
		$this->load->view("admin/aperturas/view", $data);
	}


	public function delete($id)
	{
		$data  = array(
			'estado' => "0",
		);
		$this->Aperturas_model->update($id, $data);
		echo json_encode(['sucess' => true]);
	}
}
