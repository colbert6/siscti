<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Otros extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata("login")) {
			redirect(base_url());
		}
		$this->permisos = $this->backend_lib->control();/* crear para permisos de modulos  */
		$this->load->model("Otros_model");
		$this->load->model("Pagos_model");
		$this->load->model("Estudiantes_model");
		$this->load->model("Prematriculas_model");
	}

	public function index()
	{
		$data  = array(
			'permisos' => $this->permisos, /* crear para permisos de modulos  */
			'pagos' => $this->Pagos_model->getPagos(),
		);
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/otros/listjt", $data);
		$this->load->view("layouts/footer");
		$this->load->view("content/c_otros");
	}
	public function lista()
	{
		$starIndex = $_GET['jtStartIndex'];
		$pageSize = $_GET['jtPageSize'];
		$buscar = (isset($_POST['search']) ? $_POST['search']: '' );
		$libro = $this->Otros_model->grilla($starIndex, $pageSize, $buscar);
		$jTableResult['Result'] = 'OK';
		$jTableResult['Records'] = $libro[0];
		$jTableResult['TotalRecordCount'] = $libro[1];
		header('Content-Type: application/json');
		echo json_encode($jTableResult);
	}
	public function add()
	{
		$data = array(
			"estudiantes" => $this->Estudiantes_model->getEstudiantes(),
			"conceptos" => $this->Otros_model->getConceptos(),

		);
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/otros/add", $data);
		$this->load->view("layouts/footer");
	}


	public function store()
	{
		$idusuario = $this->session->userdata("id");
		$estudiante_id = $this->input->post("idestudiante");
		$concepto_id = $this->input->post("idconcepto");
		$descripcionpago = $this->input->post("descripcionpago");
		$montopago = $this->input->post("montopago");
		$codigopago = $this->input->post("codigopago");
		$fechapago = $this->input->post("fechapago");
		//$contador = $this->input->post("contador");

		//$this->form_validation->set_rules("prematricula", "Estudiante para realizar el Pago", "required");
		$this->form_validation->set_rules("contador", "Datos del  Pago", "required");
		if ($this->form_validation->run() == TRUE) {

			if ($this->save_pago($idusuario, $estudiante_id,$concepto_id, $descripcionpago, $montopago, $codigopago, $fechapago, $deuda)) {

				redirect(base_url() . "movimientos/otros");
			} else {
				$this->session->set_flashdata("error", "No se pudo guardar la informacion");
				redirect(base_url() . "movimientos/otros/add");
			}
		} else {
			/*redirect(base_url()."mantenimiento/Niveles/add");*/
			$this->add();
		}
	}

	protected function save_pago($idusuario, $estudiante_id, $concepto_id, $descripcionpago, $montopago, $codigopago, $fechapago, $deuda)
	{
		//$sumapago = 0;
		for ($i = 0; $i < count($codigopago); $i++) {
			//$sumapago = $sumapago + $montopago[$i];
			$data  = array(
				'usuario_id' => $idusuario,
				'estudiante_id' => $estudiante_id[$i],
				'concepto_id' => $concepto_id[$i],
				'fecha_registro' => date('Y-m-d'),
				'descripcion' => $descripcionpago[$i],
				'monto' => $montopago[$i],
				'codigo' => $codigopago[$i],
				'fecha_pago' => $fechapago[$i],
				'estado' => '1',
			);
			$this->Otros_model->save($data);
			// $this->Pagos_model->GuadarDeuda($prematricula_id,$pagodeuda);
		}
	//	$totaldeuda = $deuda - $sumapago;
	//	$this->Pagos_model->GuadarPago($prematricula_id, $totaldeuda);
		redirect(base_url() . "movimientos/otros");
	}



	public function edit($id)
	{
		$data  = array(
			'pago' => $this->Otros_model->getPago($id),
			"conceptos" => $this->Otros_model->getConceptos(),
		);
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/otros/edit", $data);
		$this->load->view("layouts/footer");
	}


	public function update()
	{	
		$idusuario = $this->session->userdata("id");
		$idpago = $this->input->post("idpago");
		$concepto_id = $this->input->post("idconcepto");
		$monto = $this->input->post("monto");
		$descripcionpago = $this->input->post("descripcion");
		$codigopago = $this->input->post("codigo");
		$fechapago = $this->input->post("fecha_pago");

		// $this->form_validation->set_rules("monto","Monto del Pago","required");
		// $this->form_validation->set_rules("fechapago","Fecha del Pago","required");

		// if ($this->form_validation->run()==TRUE) {

			$data  = array(
				'usu_mod' => $idusuario, 
				'fecha_mod' => date('Y-m-d'), 
				'concepto_id' => $concepto_id, 
				'descripcion' => $descripcionpago, 
				'monto' => $monto, 
				'codigo' => $codigopago,
				'fecha_pago' => $fechapago, 
			);
		
			if ($this->Otros_model->update($idpago,$data)) {
				redirect(base_url()."movimientos/otros");
			}
			else{
				$this->session->set_flashdata("error","No se pudo Actualizar la informacion");
				redirect(base_url()."movimientos/otros/edit/".$idpago);
			}
		// }
		// else{
		// 	/*redirect(base_url()."mantenimiento/Niveles/add");*/
		// 	$this->edit($idpago);
		// }
	}


	public function view($id)
	{
		$data  = array(
			'pagocuo' => $this->Otros_model->getPago($id),
	
		);
		$this->load->view("admin/otros/view", $data);
	}

	public function delete($id)
	{
		$this->Otros_model->delete($id);
		echo json_encode(['sucess' => true]);
	}
}
