<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cursos extends CI_Controller {
	private $permisos; /* crear para permisos de modulos  */

	public function __construct(){
		parent::__construct();
		$this->permisos = $this->backend_lib->control();/* crear para permisos de modulos  */
		$this->load->model("Cursos_model");
		$this->load->helper(array('download'));
	}

	
	public function index()
	{
		$data  = array(
			'permisos' => $this->permisos, /* crear para permisos de modulos  */
			'cursos' => $this->Cursos_model->getCursos(), 
		
		);
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/cursos/listjt",$data);
		$this->load->view("layouts/footer");
		$this->load->view("content/c_cursos");
	}

	public function lista()
	{
		$starIndex = $_GET['jtStartIndex'];
		$pageSize = $_GET['jtPageSize'];
		$buscar = (isset($_POST['search']) ? $_POST['search']: '' );
		$libro = $this->Cursos_model->grilla($starIndex, $pageSize, $buscar);
		$jTableResult['Result'] = 'OK';
		$jTableResult['Records'] = $libro[0];
		$jTableResult['TotalRecordCount'] = $libro[1];
		header('Content-Type: application/json');
		echo json_encode($jTableResult);

	}


	public function add(){
		$data = array(
			"tipocursos" => $this->Cursos_model->getTipoCursos()
		);
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/cursos/add", $data);
		$this->load->view("layouts/footer");

		//Añadir 
		$this->load->view("layouts/footer_add");
	}

	public function store(){
		
		$nombre = $this->input->post("nombre");
		$costo = $this->input->post("costo");
		$tipocurso = $this->input->post("tipocurso");
		$inscripcion_web = $this->input->post("inscripcion_web");
		$this->form_validation->set_rules("nombre","Nombre del Curso","required");
		$this->form_validation->set_rules("costo","Costo del Curso","required");
		//$this->form_validation->set_rules("silabo","silabo del Curso","required");
	
		if ($this->form_validation->run()==TRUE) {
			
			$config['upload_path'] = './uploads';
			$config['allowed_types'] = 'pdf|xlsx|docx';
			$config['max_size'] = '20048';
			
			$this->load->library('upload');
			$this->upload->initialize($config);

			if(!$this->upload->do_upload("silabo")){
				//echo $this->upload->display_errors();die;
			}

			$file_info = $this->upload->data(); 
			$archivo = $file_info['file_name'];	
			
			$data  = array(
				'nombre' => $nombre,
				'costo' => $costo,
				'silabo' =>$archivo,
				'tipocurso_id'=>$tipocurso,
				'estado' => "1",
				'inscripcion_web' => $inscripcion_web
			);
		
			if ($this->Cursos_model->save($data)) {
				redirect(base_url()."mantenimiento/cursos");
			}
			else{
				$this->session->set_flashdata("error","No se pudo guardar la informacion");
				redirect(base_url()."mantenimiento/cursos/add");
			}
		}
		else{
			///redirect(base_url()."mantenimiento/Cursos/add");
			$this->add();
		}		
	}


	public function edit($id){
		$data  = array(
			'curso' => $this->Cursos_model->getCurso($id), 
			"tipocursos" => $this->Cursos_model->getTipoCursos()

		);
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/cursos/edit",$data);
		$this->load->view("layouts/footer");

		//Añadir 
		$this->load->view("layouts/footer_add");
	}


	public function update(){ 

		$idcurso = $this->input->post("idcurso");
		$nombre = $this->input->post("nombre");
		$costo = $this->input->post("costo");
		$tipocurso = $this->input->post("tipocurso");
		$inscripcion_web = $this->input->post("inscripcion_web");
		$cursoactual = $this->Cursos_model->getCurso($idcurso);

		$this->form_validation->set_rules("nombre","Nombre del Curso","required");
		$this->form_validation->set_rules("costo","costo del Curso","required");

		if ($this->form_validation->run()==TRUE) {

				$config['upload_path'] = './uploads';
				$config['allowed_types'] = 'pdf|xlsx|docx';
				$config['max_size'] = '20048';
				$this->load->library('upload');
				$this->upload->initialize($config);
				if(!$this->upload->do_upload("silabo")){
					//echo $this->upload->display_errors();die;
					$subir="1";
				}
				$file_info = $this->upload->data(); 
				$archivo = $file_info['file_name'];	
					
				if ($subir == "1") {
						$data  = array(
						'nombre' => $nombre, 
						'costo' => $costo,
						'tipocurso_id'=>$tipocurso,
						'inscripcion_web'=>$inscripcion_web
						);
				}else{

						$data  = array(
							'nombre' => $nombre, 
							'costo' => $costo,
							'silabo' =>$archivo,
							'tipocurso_id'=>$tipocurso,
							'inscripcion_web'=>$inscripcion_web
						);
				}

			if ($this->Cursos_model->update($idcurso,$data)) {
				redirect(base_url()."mantenimiento/cursos");
			}
			else{
				$this->session->set_flashdata("error","No se pudo Actualizar la informacion");
				redirect(base_url()."mantenimiento/cursos/edit/".$idcurso);
			}
		}
		else{
			/*redirect(base_url()."mantenimiento/Cursos/add");*/
			$this->edit($idcurso);
		}

		
	}



	public function view($id){
		$data  = array(
			'curso' => $this->Cursos_model->getCurso($id), 
		);
		$this->load->view("admin/cursos/view",$data);
	}


	public function delete($id){
		$data  = array(
			'estado' => "0", 
		);
		$this->Cursos_model->update($id,$data);
		echo json_encode(['sucess' => true]);
	}


}
