<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Docentes extends CI_Controller
{
	private $permisos; /* crear para permisos de modulos  */

	public function __construct()
	{
		parent::__construct();
		$this->permisos = $this->backend_lib->control();/* crear para permisos de modulos  */
		$this->load->model("Docentes_model");
	}


	public function index()
	{
		$data  = array(
			'permisos' => $this->permisos, /* crear para permisos de modulos  */
			'docentes' => $this->Docentes_model->getDocentes(),

		);
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/docentes/listjt", $data);
		$this->load->view("layouts/footer");
		$this->load->view("content/c_docentes");
	}

	public function lista()
	{
		$starIndex = $_GET['jtStartIndex'];
		$pageSize = $_GET['jtPageSize'];
		$buscar = (isset($_POST['search']) ? $_POST['search']: '' );
		$libro = $this->Docentes_model->grilla($starIndex, $pageSize, $buscar);
		$jTableResult['Result'] = 'OK';
		$jTableResult['Records'] = $libro[0];
		$jTableResult['TotalRecordCount'] = $libro[1];
		header('Content-Type: application/json');
		echo json_encode($jTableResult);

	}

	public function add()
	{

		$data  = array(
			"carreras" => $this->Docentes_model->getCarreras(),
			"sexos" => $this->Docentes_model->getSexos(),
			"tipodocumentos" => $this->Docentes_model->getTipoDocumentos()
		);

		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/docentes/add", $data);
		$this->load->view("layouts/footer");
	}


	public function store()
	{

		$nombre = $this->input->post("nombre");
		//$apellido = $this->input->post("apellido");
		$sexo = $this->input->post("sexo");
		$fecha_naci = $this->input->post("fecha_naci");
		$tipodocumento = $this->input->post("tipodocumento");
		$num_documento = $this->input->post("numero");
		$direccion = $this->input->post("direccion");
		$telefono = $this->input->post("telefono");
		$celular = $this->input->post("celular");
		$email = $this->input->post("email");
		$carrera = $this->input->post("carrera");

		$this->form_validation->set_rules("nombre", "Nombres del Docente", "required");
		//	$this->form_validation->set_rules("apellido","Apellido delDocente","required");
		$this->form_validation->set_rules("sexo", "Sexo del Docente", "required");
		$this->form_validation->set_rules("fecha_naci", "Fecha de Nacimiento del Docente", "required");
		$this->form_validation->set_rules("tipodocumento", "Tipo del Documento del Docente", "required");
		$this->form_validation->set_rules("numero", "Numero de Documento", "required|is_unique[docentes.num_documento]");
		$this->form_validation->set_rules("direccion", "Direccion del Docente", "required");
		$this->form_validation->set_rules("celular", "Celular del Docente", "required|is_unique[docentes.celular]");
		$this->form_validation->set_rules("email", "Correo Electronico del Docente", "required|is_unique[docentes.email]");
		$this->form_validation->set_rules("carrera", "Carrera de Nacimiento del Docente", "required");

		if ($this->form_validation->run() == TRUE) {

			$data  = array(
				'num_documento' => $num_documento,
				'tipo_documento_id' => $tipodocumento,
				'nombre' => $nombre,
				'sexo_id' => $sexo,
				'fecha_nacimiento' => $fecha_naci,
				'direccion' => $direccion,
				'telefono' => $telefono,
				'celular' => $celular,
				'email' => $email,
				'carrera_id' => $carrera,
				'fecha_registro' => date('Y-m-d'),
				'estado' => "1",

			);

			if ($this->Docentes_model->save($data)) {
				redirect(base_url() . "mantenimiento/docentes");
			} else {
				$this->session->set_flashdata("error", "No se pudo guardar la informacion");
				redirect(base_url() . "mantenimiento/docentes/add");
			}
		} else {
			/*redirect(base_url()."mantenimiento/Docentes/add");*/
			$this->add();
		}
	}


	public function edit($id)
	{
		$data  = array(
			'docente' => $this->Docentes_model->getDocente($id),
			"carreras" => $this->Docentes_model->getCarreras(),
			"sexos" => $this->Docentes_model->getSexos(),
			"tipodocumentos" => $this->Docentes_model->getTipoDocumentos()
		);
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/docentes/edit", $data);
		$this->load->view("layouts/footer");
	}


	public function update()
	{

		$iddocente = $this->input->post("iddocente");
		$nombre = $this->input->post("nombre");
		//	$apellido = $this->input->post("apellido");
		$sexo = $this->input->post("sexo");
		$fecha_naci = $this->input->post("fecha_naci");
		$tipodocumento = $this->input->post("tipodocumento");
		$num_documento = $this->input->post("numero");
		$direccion = $this->input->post("direccion");
		$telefono = $this->input->post("telefono");
		$celular = $this->input->post("celular");
		$email = $this->input->post("email");
		$carrera = $this->input->post("carrera");


		$docenteactual = $this->Docentes_model->getDocente($iddocente);

		if ($num_documento == $docenteactual->num_documento) {
			$is_unique = "";
		} else {
			$is_unique = "|is_unique[docentes.num_documento]";
		}

		$this->form_validation->set_rules("nombre", "Nombres del Docente", "required");
		//	$this->form_validation->set_rules("apellido","Apellido del Docente","required");
		$this->form_validation->set_rules("sexo", "Sexo del Docente", "required");
		$this->form_validation->set_rules("fecha_naci", "Fecha de Nacimiento del Docente", "required");
		$this->form_validation->set_rules("tipodocumento", "Tipo del Documento del Docente", "required");
		$this->form_validation->set_rules("numero", "Numero de Documento", "required" . $is_unique);
		$this->form_validation->set_rules("direccion", "Direccion del Docente", "required");
		$this->form_validation->set_rules("celular", "Celular del Docente", "required");
		$this->form_validation->set_rules("email", "Correo Electronico del Docente", "required");
		$this->form_validation->set_rules("carrera", "Carrera de Nacimiento del Docente", "required");

		if ($this->form_validation->run() == TRUE) {

			$data  = array(
				'num_documento' => $num_documento,
				'tipo_documento_id' => $tipodocumento,
				'nombre' => $nombre,
				'sexo_id' => $sexo,
				'fecha_nacimiento' => $fecha_naci,
				'direccion' => $direccion,
				'telefono' => $telefono,
				'celular' => $celular,
				'email' => $email,
				'carrera_id' => $carrera,
				'estado' => "1",

			);

			if ($this->Docentes_model->update($iddocente, $data)) {
				redirect(base_url() . "mantenimiento/docentes");
			} else {
				$this->session->set_flashdata("error", "No se pudo Actualizar la informacion");
				redirect(base_url() . "mantenimiento/docentes/edit/" . $iddocente);
			}
		} else {
			/*redirect(base_url()."mantenimiento/Docentes/add");*/
			$this->edit($iddocente);
		}
	}

	public function view($id)
	{
		$data  = array(
			'docente' => $this->Docentes_model->getVer($id),
		);
		$this->load->view("admin/docentes/view", $data);
	}


	public function delete($id)
	{
		$data  = array(
			'estado' => "0",
		);
		$this->Docentes_model->update($id, $data);
		echo json_encode(['sucess' => true]);
	}
}
