<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Estudiantes extends CI_Controller
{
	private $permisos; /* crear para permisos de modulos  */

	public function __construct()
	{
		parent::__construct();
		$this->permisos = $this->backend_lib->control();/* crear para permisos de modulos  */
		$this->load->model("Estudiantes_model");
		
	}


	public function index()
	{
		$data  = array(
			'permisos' => $this->permisos, /* crear para permisos de modulos  */
			'estudiantes' => $this->Estudiantes_model->getEstudiantes(),

		);
		
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/estudiantes/listjt", $data);
		$this->load->view("layouts/footer");
		$this->load->view("content/c_estudiantes");
		
	}

	public function lista()
	{
		$starIndex = $_GET['jtStartIndex'];
		$pageSize = $_GET['jtPageSize'];
		$buscar = (isset($_POST['search']) ? $_POST['search']: '' );
		$libro = $this->Estudiantes_model->grilla($starIndex, $pageSize, $buscar);
		$jTableResult['Result'] = 'OK';
		$jTableResult['Records'] = $libro[0];
		$jTableResult['TotalRecordCount'] = $libro[1];
		header('Content-Type: application/json');
		echo json_encode($jTableResult);

	}


	public function create(){
		$id=$this->Estuiantes_model->create($_POST);
		if($id==0){
			$jTableResult['Result'] = 'ERROR';
			$jTableResult['Message']= 'No se Inserto';
		}else
		{
			$libro=$this->Estuidantes_model->one($id);
			$jTableResult['Result'] = 'ERROR';
			$jTableResult['Record']= $libro;
		}
		header('Content-Type: application/json');
		echo json_encode($jTableResult);
	}


	public function getDNI()
	{	$numero = $this->input->post("documento");
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://aplicaciones007.jne.gob.pe/srop_publico/Consulta/api/AfiliadoApi/GetNombresCiudadano",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "CODDNI=$numero",
		  CURLOPT_HTTPHEADER => array(
			"Requestverificationtoken: Dmfiv1Unnsv8I9EoXEzbyQExSD8Q1UY7viyyf_347vRCfO-1xGFvDddaxDAlvm0cZ8XgAKTaWclVFnnsGgoy4aLlBGB5m-E8rGw_ymEcCig1:eq4At-H2zqgXPrPnoiDGFZH0Fdx5a-1UiyVaR4nQlCvYZzAhzmvWxLwkUk6-yORYrBBxEnoG5sm-Hkiyc91so6-nHHxIeLee5p700KE47Cw1",
			"Content-Type: application/x-www-form-urlencoded",
			"Cookie: ASP.NET_SessionId=zjlc4c43wx03oqtdg45t05fn"
		  ),
		));
		
		$response = curl_exec($curl);
		
		curl_close($curl);
		echo $response;
		
	}

	public function getRUC()
	{	$numero = $this->input->post("documento");
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => "http://services.wijoata.com/consultar-ruc/api/ruc/$numero",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_HTTPHEADER => array(
			"Accept: application/json",
			"Content-Type: application/json"
		),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		echo $response;
	}

	public function add()
	{

		$data  = array(
			"carreras" => $this->Estudiantes_model->getCarreras(),
			"sexos" => $this->Estudiantes_model->getSexos(),
			"tipodocumentos" => $this->Estudiantes_model->getTipoDocumentos()
		);

		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/estudiantes/add", $data);
		$this->load->view("layouts/footer");
	}

	public function addpre()
	{

		$data  = array(
			"carreras" => $this->Estudiantes_model->getCarreras(),
			"sexos" => $this->Estudiantes_model->getSexos(),
			"tipodocumentos" => $this->Estudiantes_model->getTipoDocumentos()
		);

		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/estudiantes/addpre", $data);
		$this->load->view("layouts/footer");
	}



	public function store()
	{

		$nombre = $this->input->post("nombre");
		//	$apellido = $this->input->post("apellido");
		$sexo = $this->input->post("sexo");
		$fecha_naci = $this->input->post("fecha_naci");
		$tipodocumento = $this->input->post("tipodocumento");
		$num_documento = $this->input->post("numero");
		$direccion = $this->input->post("direccion");
		$telefono = $this->input->post("telefono");
		$celular = $this->input->post("celular");
		$email = $this->input->post("email");
		$carrera = $this->input->post("carrera");

		$this->form_validation->set_rules("nombre", "Nombres del Estudiante", "required");
		//$this->form_validation->set_rules("apellido","Apellido delEstudiante","required");
		$this->form_validation->set_rules("sexo", "Sexo del Estudiante", "required");
	//	$this->form_validation->set_rules("fecha_naci", "Fecha de Nacimiento del Estudiante", "required");
		$this->form_validation->set_rules("tipodocumento", "Tipo del Documento del Estudiante", "required");
		$this->form_validation->set_rules("numero", "Numero de Documento", "required|is_unique[estudiantes.num_documento]");
	//	$this->form_validation->set_rules("direccion", "Direccion del Estudiante", "required");
	//	$this->form_validation->set_rules("celular", "Celular del Estudiante", "required|is_unique[estudiantes.celular]");
	//	$this->form_validation->set_rules("email", "Correo Electronico del Estudiante", "required|is_unique[estudiantes.email]");
	//	$this->form_validation->set_rules("carrera", "Carrera de Nacimiento del Estudiante", "required");

		if ($this->form_validation->run() == TRUE) {

			$data  = array(
				'num_documento' => $num_documento,
				'tipo_documento_id' => $tipodocumento,
				'nombre' => $nombre,
				'sexo_id' => $sexo,
				'fecha_nacimiento' => $fecha_naci,
				'direccion' => $direccion,
				'telefono' => $telefono,
				'celular' => $celular,
				'email' => $email,
				'carrera_id' => $carrera,
				'fecha_registro' => date('Y-m-d'),
				'estado' => "1",

			);

			if ($this->Estudiantes_model->save($data)) {
				redirect(base_url() . "mantenimiento/estudiantes");
			} else {
				$this->session->set_flashdata("error", "No se pudo guardar la informacion");
				redirect(base_url() . "mantenimiento/estudiantes/add");
			}
		} else {
			/*redirect(base_url()."mantenimiento/estudiantes/add");*/
			$this->add();
		}
	}

	public function storepre()
	{

		$nombre = $this->input->post("nombre");
		//	$apellido = $this->input->post("apellido");
		$sexo = $this->input->post("sexo");
		$fecha_naci = $this->input->post("fecha_naci");
		$tipodocumento = $this->input->post("tipodocumento");
		$num_documento = $this->input->post("numero");
		$direccion = $this->input->post("direccion");
		$telefono = $this->input->post("telefono");
		$celular = $this->input->post("celular");
		$email = $this->input->post("email");
		$carrera = $this->input->post("carrera");

		$this->form_validation->set_rules("nombre", "Nombres del Estudiante", "required");
		//$this->form_validation->set_rules("apellido","Apellido delEstudiante","required");
		$this->form_validation->set_rules("sexo", "Sexo del Estudiante", "required");
	//	$this->form_validation->set_rules("fecha_naci", "Fecha de Nacimiento del Estudiante", "required");
		$this->form_validation->set_rules("tipodocumento", "Tipo del Documento del Estudiante", "required");
		$this->form_validation->set_rules("numero", "Numero de Documento", "required|is_unique[estudiantes.num_documento]");
	//	$this->form_validation->set_rules("direccion", "Direccion del Estudiante", "required");
	//	$this->form_validation->set_rules("celular", "Celular del Estudiante", "required|is_unique[estudiantes.celular]");
	//	$this->form_validation->set_rules("email", "Correo Electronico del Estudiante", "required|is_unique[estudiantes.email]");
	//	$this->form_validation->set_rules("carrera", "Carrera de Nacimiento del Estudiante", "required");

		if ($this->form_validation->run() == TRUE) {

			$data  = array(
				'num_documento' => $num_documento,
				'tipo_documento_id' => $tipodocumento,
				'nombre' => $nombre,
				'sexo_id' => $sexo,
				'fecha_nacimiento' => $fecha_naci,
				'direccion' => $direccion,
				'telefono' => $telefono,
				'celular' => $celular,
				'email' => $email,
				'carrera_id' => $carrera,
				'fecha_registro' => date('Y-m-d'),
				'estado' => "1",

			);

			if ($this->Estudiantes_model->save($data)) {
				redirect(base_url() . "movimientos/prematriculas/add");
			} else {
				$this->session->set_flashdata("error", "No se pudo guardar la informacion");
				redirect(base_url() . "mantenimiento/estudiantes/addpre");
			}
		} else {
			/*redirect(base_url()."mantenimiento/estudiantes/add");*/
			$this->addpre();
		}
	}



	public function edit($id)
	{
		$data  = array(
			'estudiante' => $this->Estudiantes_model->getEstudiante($id),
			"carreras" => $this->Estudiantes_model->getCarreras(),
			"sexos" => $this->Estudiantes_model->getSexos(),
			"tipodocumentos" => $this->Estudiantes_model->getTipoDocumentos()
		);
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/estudiantes/edit", $data);
		$this->load->view("layouts/footer");
	}


	public function update()
	{

		$idestudiante = $this->input->post("idestudiante");
		$nombre = $this->input->post("nombre");
		//	$apellido = $this->input->post("apellido");
		$sexo = $this->input->post("sexo");
		$fecha_naci = $this->input->post("fecha_naci");
		$tipodocumento = $this->input->post("tipodocumento");
		$num_documento = $this->input->post("numero");
		$direccion = $this->input->post("direccion");
		$telefono = $this->input->post("telefono");
		$celular = $this->input->post("celular");
		$email = $this->input->post("email");
		$carrera = $this->input->post("carrera");


		$estudianteactual = $this->Estudiantes_model->getEstudiante($idestudiante);

		if ($num_documento == $estudianteactual->num_documento) {
			$is_unique = "";
		} else {
			$is_unique = "|is_unique[estudiantes.num_documento]";
		}

		$this->form_validation->set_rules("nombre", "Nombres del Estudiante", "required");
		//$this->form_validation->set_rules("apellido","Apellido delEstudiante","required");
		$this->form_validation->set_rules("sexo", "Sexo del Estudiante", "required");
		//$this->form_validation->set_rules("fecha_naci", "Fecha de Nacimiento del Estudiante", "required");
		$this->form_validation->set_rules("tipodocumento", "Tipo del Documento del Estudiante", "required");
		$this->form_validation->set_rules("numero", "Numero de Documento", "required" . $is_unique);
		//$this->form_validation->set_rules("direccion", "Direccion del Estudiante", "required");
		$this->form_validation->set_rules("celular", "Celular del Estudiante", "required");
		//$this->form_validation->set_rules("email", "Correo Electronico del Estudiante", "required");
		$this->form_validation->set_rules("carrera", "Carrera de Nacimiento del Estudiante", "required");

		if ($this->form_validation->run() == TRUE) {

			$data  = array(
				'num_documento' => $num_documento,
				'tipo_documento_id' => $tipodocumento,
				'nombre' => $nombre,
				'sexo_id' => $sexo,
				'fecha_nacimiento' => $fecha_naci,
				'direccion' => $direccion,
				'telefono' => $telefono,
				'celular' => $celular,
				'email' => $email,
				'carrera_id' => $carrera,
				//'fecha_registro' => date('Y-m-d'),
				'estado' => "1",

			);

			if ($this->Estudiantes_model->update($idestudiante, $data)) {
				redirect(base_url() . "mantenimiento/estudiantes");
			} else {
				$this->session->set_flashdata("error", "No se pudo Actualizar la informacion");
				redirect(base_url() . "mantenimiento/estudiantes/edit/" . $idestudiante);
			}
		} else {
			/*redirect(base_url()."mantenimiento/estudiantes/add");*/
			$this->edit($idestudiante);
		}
	}

	public function view($id)
	{
		$data  = array(
			'estudiante' => $this->Estudiantes_model->getVer($id),
		);
		$this->load->view("admin/estudiantes/view", $data);
	}


	public function delete($id)
	{
		$data  = array(
			'estado' => "0",
		);
		$this->Estudiantes_model->update($id, $data);
		echo json_encode(['sucess' => true]);
	}
}
