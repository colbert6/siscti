<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aulas extends CI_Controller {
	private $permisos; /* crear para permisos de modulos  */

	public function __construct(){
		parent::__construct();
		$this->permisos = $this->backend_lib->control();/* crear para permisos de modulos  */
		$this->load->model("Aulas_model");
	}

	
	public function index()
	{
		$data  = array(
			'permisos' => $this->permisos, /* crear para permisos de modulos  */
			'aulas' => $this->Aulas_model->getAulas(), 
		
		);
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/aulas/listjt",$data);
		$this->load->view("layouts/footer");
		$this->load->view("content/c_aulas");

	}
	public function lista()
	{
		$starIndex = $_GET['jtStartIndex'];
		$pageSize = $_GET['jtPageSize'];
		$buscar = (isset($_POST['search']) ? $_POST['search']: '' );
		$libro = $this->Aulas_model->grilla($starIndex, $pageSize, $buscar);
		$jTableResult['Result'] = 'OK';
		$jTableResult['Records'] = $libro[0];
		$jTableResult['TotalRecordCount'] = $libro[1];
		header('Content-Type: application/json');
		echo json_encode($jTableResult);
	}

	public function add(){

		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/aulas/add");
		$this->load->view("layouts/footer");
	}


	public function store(){ 

		$nombre = $this->input->post("nombre");
		$descuento = $this->input->post("descuento");
		

		$this->form_validation->set_rules("nombre","Nombre del aula","required|is_unique[aulas.nombre]");
		//$this->form_validation->set_rules("descuento","Descuento del aula","required");
	
		if ($this->form_validation->run()==TRUE) {

			$data  = array(
				'nombre' => $nombre, 
				//'descuento' => $descuento,
				
				'estado' => "1"
			);
		
			if ($this->Aulas_model->save($data)) {
				redirect(base_url()."mantenimiento/aulas");
			}
			else{
				$this->session->set_flashdata("error","No se pudo guardar la informacion");
				redirect(base_url()."mantenimiento/aulas/add");
			}
		}
		else{
			/*redirect(base_url()."mantenimiento/aulas/add");*/
			$this->add();
		}

		
	}


	public function edit($id){
		
		$data  = array(
			'aula' => $this->Aulas_model->getAula($id), 
		);
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/aulas/edit",$data);
		$this->load->view("layouts/footer");
	}


	public function update(){ 

		$idaula = $this->input->post("idaula");
		$nombre = $this->input->post("nombre");
	//	$descuento = $this->input->post("descuento");
	
		$aulaactual = $this->Aulas_model->getAula($idaula);

		if ($nombre == $aulaactual->nombre) {
			$is_unique = "";
		}else{
			$is_unique = "|is_unique[aulas.nombre]";

		}

		$this->form_validation->set_rules("nombre","Nombre del aula","required".$is_unique);
		//$this->form_validation->set_rules("descuento","Descuento del aula","required");

		if ($this->form_validation->run()==TRUE) {

			$data  = array(
				'nombre' => $nombre, 
				//'descuento' => $descuento,
				
			);
		
			if ($this->Aulas_model->update($idaula,$data)) {
				redirect(base_url()."mantenimiento/aulas");
			}
			else{
				$this->session->set_flashdata("error","No se pudo Actualizar la informacion");
				redirect(base_url()."mantenimiento/aulas/edit/".$idaula);
			}
		}
		else{
			/*redirect(base_url()."mantenimiento/aulas/add");*/
			$this->edit($idaula);
		}

		
	}

	public function view($id){

		$data  = array(
			'aula' => $this->Aulas_model->getAula($id), 
		);
		$this->load->view("admin/aulas/view",$data);
	}


	public function delete($id){

		$data  = array(
			'estado' => "0", 
		);
		$this->Aulas_model->update($id,$data);
		echo json_encode(['sucess' => true]);
	}
}
