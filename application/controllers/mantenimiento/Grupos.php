<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupos extends CI_Controller {
	private $permisos; /* crear para permisos de modulos  */

	public function __construct(){
		parent::__construct();
		$this->permisos = $this->backend_lib->control();/* crear para permisos de modulos  */
		$this->load->model("Grupos_model");
	}

	
	public function index()
	{
		$data  = array(
			'permisos' => $this->permisos, /* crear para permisos de modulos  */
			'grupos' => $this->Grupos_model->getGrupos(), 
		
		);
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/grupos/listjt",$data);
		$this->load->view("layouts/footer");
		$this->load->view("content/c_grupos");

	}

	public function lista()
	{
		$starIndex = $_GET['jtStartIndex'];
		$pageSize = $_GET['jtPageSize'];
		$buscar = (isset($_POST['search']) ? $_POST['search']: '' );
		$libro = $this->Grupos_model->grilla($starIndex, $pageSize, $buscar);
		$jTableResult['Result'] = 'OK';
		$jTableResult['Records'] = $libro[0];
		$jTableResult['TotalRecordCount'] = $libro[1];
		header('Content-Type: application/json');
		echo json_encode($jTableResult);
	}

	public function add(){

		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/grupos/add");
		$this->load->view("layouts/footer");
	}


	public function store(){ 

		$nombre = $this->input->post("nombre");
		$hora_ini = $this->input->post("hora_ini");
		$hora_fin = $this->input->post("hora_fin");
		$comentario = $this->input->post("comentario");

		$this->form_validation->set_rules("nombre","Nombre del Grupo","required");
		$this->form_validation->set_rules("hora_ini","Hora de Inicio del Grupo","required");
		$this->form_validation->set_rules("hora_fin","Hora de Fin del Grupo","required");
		$this->form_validation->set_rules("comentario","comentario del Grupo","required");
	
		if ($this->form_validation->run()==TRUE) {

			$data  = array(
				'nombre' => $nombre, 
				'hora_ini' => $hora_ini,
				'hora_fin' => $hora_fin,
				'comentario' => $comentario,
				'estado' => "1"
			);
		
			if ($this->Grupos_model->save($data)) {
				redirect(base_url()."mantenimiento/grupos");
			}
			else{
				$this->session->set_flashdata("error","No se pudo guardar la informacion");
				redirect(base_url()."mantenimiento/grupos/add");
			}
		}
		else{
			/*redirect(base_url()."mantenimiento/Grupos/add");*/
			$this->add();
		}

		
	}


	public function edit($id){
		$data  = array(
			'grupo' => $this->Grupos_model->getgrupo($id), 
		);
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/grupos/edit",$data);
		$this->load->view("layouts/footer");
	}

	public function update(){ 

		$idgrupo = $this->input->post("idgrupo");
		$nombre = $this->input->post("nombre");
		$hora_ini = $this->input->post("hora_ini");
		$hora_fin = $this->input->post("hora_fin");
		$comentario = $this->input->post("comentario");
		$this->form_validation->set_rules("nombre","Nombre del Grupo","required");
		$this->form_validation->set_rules("hora_ini","Hora de Inicio del Grupo","required");
		$this->form_validation->set_rules("hora_fin","Hora de Fin del Grupo","required");
		$this->form_validation->set_rules("comentario","comentario del Grupo","required");

		if ($this->form_validation->run()==TRUE) {

			$data  = array(
				'nombre' => $nombre, 
				'hora_ini' => $hora_ini,
				'hora_fin' => $hora_fin,
				'comentario' => $comentario,
			);
		
			if ($this->Grupos_model->update($idgrupo,$data)) {
				redirect(base_url()."mantenimiento/grupos");
			}
			else{
				$this->session->set_flashdata("error","No se pudo Actualizar la informacion");
				redirect(base_url()."mantenimiento/grupos/edit/".$idgrupo);
			}
		}
		else{
			/*redirect(base_url()."mantenimiento/Grupos/add");*/
			$this->edit($idgrupo);
		}

		
	}

	public function view($id){
		$data  = array(
			'grupo' => $this->Grupos_model->getGrupo($id), 
		);
		$this->load->view("admin/grupos/view",$data);
	}


	public function delete($id){
		$data  = array(
			'estado' => "0", 
		);
		$this->Grupos_model->update($id,$data);
		echo json_encode(['sucess' => true]);
	}

}
