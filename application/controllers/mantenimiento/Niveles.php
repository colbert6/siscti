<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Niveles extends CI_Controller {
	private $permisos; /* crear para permisos de modulos  */

	public function __construct(){
		parent::__construct();
		$this->permisos = $this->backend_lib->control();/* crear para permisos de modulos  */
		$this->load->model("Niveles_model");
	}

	
	public function index()
	{
		$data  = array(
			'permisos' => $this->permisos, /* crear para permisos de modulos  */
			'niveles' => $this->Niveles_model->getNiveles(), 
		
		);
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/niveles/listjt",$data);
		$this->load->view("layouts/footer");
		$this->load->view("content/c_niveles");

	}

	public function lista()
	{
		$starIndex = $_GET['jtStartIndex'];
		$pageSize = $_GET['jtPageSize'];
		$buscar = (isset($_POST['search']) ? $_POST['search']: '' );
		$libro = $this->Niveles_model->grilla($starIndex, $pageSize, $buscar);
		$jTableResult['Result'] = 'OK';
		$jTableResult['Records'] = $libro[0];
		$jTableResult['TotalRecordCount'] = $libro[1];
		header('Content-Type: application/json');
		echo json_encode($jTableResult);
	}
	public function add(){

		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/niveles/add");
		$this->load->view("layouts/footer");
	}


	public function store(){ 

		$nombre = $this->input->post("nombre");
		$descuento = $this->input->post("descuento");
		$this->form_validation->set_rules("nombre","Nombre del Nivel","required|is_unique[niveles.nombre]");
		$this->form_validation->set_rules("descuento","Descuento del Nivel","required");
	
		if ($this->form_validation->run()==TRUE) {

			$data  = array(
				'nombre' => $nombre, 
				'descuento' => $descuento,	
				'estado' => "1"
			);
		
			if ($this->Niveles_model->save($data)) {
				redirect(base_url()."mantenimiento/niveles");
			}
			else{
				$this->session->set_flashdata("error","No se pudo guardar la informacion");
				redirect(base_url()."mantenimiento/niveles/add");
			}
		}
		else{
			/*redirect(base_url()."mantenimiento/Niveles/add");*/
			$this->add();
		}

		
	}


	public function edit($id){
		$data  = array(
			'nivel' => $this->Niveles_model->getnivel($id), 
		);
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/niveles/edit",$data);
		$this->load->view("layouts/footer");
	}


	public function update(){ 

		$idnivel = $this->input->post("idnivel");
		$nombre = $this->input->post("nombre");
		$descuento = $this->input->post("descuento");
		$nivelactual = $this->Niveles_model->getnivel($idnivel);
		if ($nombre == $nivelactual->nombre) {
			$is_unique = "";
		}else{
			$is_unique = "|is_unique[niveles.nombre]";

		}
		$this->form_validation->set_rules("nombre","Nombre del Nivel","required".$is_unique);
		$this->form_validation->set_rules("descuento","Descuento del Nivel","required");

		if ($this->form_validation->run()==TRUE) {

			$data  = array(
				'nombre' => $nombre, 
				'descuento' => $descuento,	
			);
		
			if ($this->Niveles_model->update($idnivel,$data)) {
				redirect(base_url()."mantenimiento/niveles");
			}
			else{
				$this->session->set_flashdata("error","No se pudo Actualizar la informacion");
				redirect(base_url()."mantenimiento/niveles/edit/".$idnivel);
			}
		}
		else{
			/*redirect(base_url()."mantenimiento/Niveles/add");*/
			$this->edit($idnivel);
		}

		
	}

	public function view($id){
		$data  = array(
			'nivel' => $this->Niveles_model->getNivel($id), 
		);
		$this->load->view("admin/niveles/view",$data);
	}
	public function delete($id){
		$data  = array(
			'estado' => "0", 
		);
		$this->Niveles_model->update($id,$data);
		echo json_encode(['sucess' => true]);
	}
}
