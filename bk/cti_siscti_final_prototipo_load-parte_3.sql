
ALTER TABLE `cursos` ADD `inscripcion_web` VARCHAR(15) NOT NULL DEFAULT 'no mostrar' AFTER `estado`;

ALTER TABLE `aperturas` ADD `estado_inscripcion` VARCHAR(15) NOT NULL DEFAULT 'abierto' AFTER `estado`;


CREATE TABLE `log` (
  `log_id` int(10) NOT NULL,
  `prematricula_id` int(10) DEFAULT NULL,
  `check_anterior` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ip_maquina` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=DYNAMIC;


INSERT INTO `menus` (`id`, `nombre`, `link`) VALUES
(24, 'Eventos', 'web_admin/eventos'),
(25, 'Cursos web', 'web_admin/cursos'),
(26, 'Organizacion', 'web_admin/organizacion'),
(27, 'Servicios', 'web_admin/servicios'),
(28, 'solicitudes', 'web_admin/solicitudes');

INSERT INTO `menus` (`id`, `nombre`, `link`) VALUES
(29, 'Pagos', 'web_admin/pagos');

INSERT INTO `permisos` (`id`, `menu_id`, `rol_id`, `read`, `insert`, `update`, `delete`) VALUES
(62, 24, 1, 1, 1, 1, 1),
(63, 25, 1, 1, 1, 1, 1),
(64, 26, 1, 1, 1, 1, 1),
(65, 27, 1, 1, 1, 1, 1),
(67, 28, 1, 1, 1, 1, 1);
INSERT INTO `permisos` (`id`, `menu_id`, `rol_id`, `read`, `insert`, `update`, `delete`) VALUES
(68, 29, 1, 1, 1, 1, 1);


CREATE TABLE `web_cursos` (
  `curso_id` int(11) NOT NULL COMMENT 'Id del registro',
  `codigo_curso` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Codigo asignado por el administrador',
  `nombre_curso` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion_curso` mediumtext COLLATE utf8_spanish_ci DEFAULT NULL,
  `abreviatura_curso` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `icono_curso` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagen_curso` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `enlace_web_curso` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `enlace_web_informacion_curso` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` enum('activo','inactivo') COLLATE utf8_spanish_ci DEFAULT 'activo',
  `inscripcion_web` varchar(15) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'no mostrar',
  `fecha_creacion` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

CREATE TABLE `web_cursos_contenido` (
  `curso_contenido_id` int(10) NOT NULL,
  `curso_id` int(10) DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE `web_cursos_contenido_det` (
  `curso_contenido_det_id` int(255) NOT NULL,
  `curso_id` int(10) DEFAULT NULL,
  `curso_contenido_id` int(10) DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE `web_cursos_icono` (
  `curso_icono_id` int(10) NOT NULL,
  `curso_id` int(10) DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `icono` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=DYNAMIC;

ALTER TABLE `web_cursos_icono` ADD `color` VARCHAR(8) NOT NULL DEFAULT '#0089ff' AFTER `icono`;

CREATE TABLE `web_cursos_icono_det` (
  `curso_icono_det_id` int(10) NOT NULL,
  `curso_icono_id` int(10) DEFAULT NULL,
  `curso_id` int(10) DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `orden` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE `web_cursos_modulo` (
  `curso_modulo_id` int(10) NOT NULL,
  `curso_id` int(10) DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE `web_cursos_modulo_det` (
  `curso_modulo_det_id` int(10) NOT NULL,
  `curso_id` int(10) DEFAULT NULL,
  `curso_modulo_id` int(10) DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE `web_eventos` (
  `evento_id` int(11) NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `titulo_evento` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `subtitulo_evento` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `descripcion_evento` mediumtext COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagen_evento` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

INSERT INTO `web_eventos` (`evento_id`, `fecha_inicio`, `fecha_fin`, `titulo_evento`, `subtitulo_evento`, `descripcion_evento`, `imagen_evento`, `estado`, `fecha_creacion`) VALUES
(1, '2020-11-11', '0000-00-00', 'PROGRAMA DE FORMACIÓN BÁSICA DE CIENCIA DE DATOS', 'Nuevo curso de Ciencia de Datos, dirigido a docentes, investigadores y profesionales en general.', 'Aprende la ciencia que te lleva al siguiente nivel en tendencias, conoce cómo identificarlas y aprovecharlas para tu negocio, desarrollo profesional o personal.\r\n<a href=\'\'https://ctiunsm.pe/registro/\"> Registro enlace</a>', 'WhatsApp_Image_2020-11-07_at_1_37_18_PM.jpeg', '1', '2020-11-07 21:01:04');


CREATE TABLE `web_organizacion` (
  `id_organizacion` int(11) NOT NULL,
  `introduccion_organizacion` mediumtext COLLATE utf8_spanish_ci NOT NULL,
  `organigrama` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `vision_organizacion` mediumtext COLLATE utf8_spanish_ci NOT NULL,
  `mision_organizacion` mediumtext COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `direccion_referencia` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono_principal` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefonos` mediumtext COLLATE utf8_spanish_ci DEFAULT NULL,
  `correo_principal` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `correos` mediumtext COLLATE utf8_spanish_ci DEFAULT NULL,
  `informacion_pago` mediumtext COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;


INSERT INTO `web_organizacion` (`id_organizacion`, `introduccion_organizacion`, `organigrama`, `vision_organizacion`, `mision_organizacion`, `direccion`, `direccion_referencia`, `telefono_principal`, `telefonos`, `correo_principal`, `correos`, `informacion_pago`) VALUES
(1, 'La creación del centro de producción de la Facultad de Ingeniería de Sistemas e Informática fue aprobada con resolución N°026-2003-UNSM/CRYG-ANR. Donde se otorga la administración del centro a la FISI.\r\n\r\nEl estatuto de la Universidad Nacional de San Martin -Tarapoto, aprobó con resolución N° 005-2016/AUR/NLU del 15.02.2016, con articulo 21° indica: las facultades, para el mejor cumplimiento de sus funciones, (⬦). Asimismo, Formara comisiones Permanentes y/o Especiales, cuando el Consejo de Facultad lo Requiera, las cuales rendirán cuentas a su consejo del cumplimiento de sus tareas.\r\n\r\nLa comisión de Administración del CTI está conformada por los docentes de la FISI, los cuales son encargados de velar por el normal desarrollo del Centro, su mejora continua, proyección a la comunidad y sostenibilidad en el tiempo a través de la oferta de sus servicios hacia la comunidad universitaria y a la población en general.', '', 'Al 2020, somos un Centro de Producción de la Universidad Nacional de San Martín-T, líder en el campo de servicios de capacitación en TIC en la amazonia peruana, con tecnologías de tendencias actuales que conlleven a la calidad educativa esperada al servicio de la sociedad.', 'Somos un Centro de Producción de la Universidad Nacional de San Martín-T, administrada por la Facultad de Ingeniería de Sistemas e Informática, dedicada a brindar servicios de capacitación en TIC a la comunidad universitaria y público en general; mejorando la competitividad y creatividad de nuestros estudiantes y respondan a las exigencias del mercado, integrando y complementado su formación académica.', 'Jr. Orellana 575 - Tarapoto', '(En las intalaciones del complejo universitario de la UNSM-T)', '955 941 992', '042 -480142, 955 941 992 , 944 929 637', 'ctiunsm@unsm.edu.pe', 'ctiunsm@unsm.edu.pe, ctiunsm@gmail.com', 'Se debe realizar el pago de matrícula en:\r\nBanco de la Nación en la Cta.:  00111\r\nConcepto de Pago: -.\r\n\r\n\r\nFinalmente, traer el recibo para reservar el cupo.');


CREATE TABLE `web_pagos` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `prematricula_id` int(11) DEFAULT NULL,
  `fecha_registro` date DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `monto` decimal(10,2) DEFAULT NULL,
  `codigo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha_pago` date DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL,
  `imagen` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `comentario` varchar(600) COLLATE utf8_spanish_ci DEFAULT NULL,
  `pago_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=DYNAMIC;

ALTER TABLE `web_pagos` CHANGE `fecha_registro` `fecha_registro` DATETIME NULL DEFAULT NULL;

CREATE TABLE `web_servicios` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `text` mediumtext COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagen` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `enlace` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci DEFAULT '1',
  `fecha_creacion` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

INSERT INTO `web_servicios` (`id`, `titulo`, `text`, `imagen`, `enlace`, `estado`, `fecha_creacion`) VALUES
(1, 'EXAMEN  DE SUFICIENCIA', 'CTI - UNSM te facilita el requisito de suficiencia en cursos informáticos, a través de la administración de un examen de suficiencia.', '75702-servicios_4_.png', 'web/servicios/informacion/examen-suficiencia', '1', '2020-10-13 23:38:28'),
(2, 'CAPACITACIÓN DE EMPRESAS', 'Cursos y programas dictados por docentes especialistas, diseñados a la medida de las necesidades en capacitación informática de su organización.', '54854-servicios_5_.jpg', 'web/servicios/informacion/capacitacion-empresas', '1', '2020-10-13 23:47:46'),
(3, 'CURSOS', 'Se muestran los cursos programados para el mes actual y próximos a aperturarse.', '4ab9c-servicios_3.jpg', 'web/cursos', '1', '2020-10-13 23:52:17');


CREATE TABLE `web_solicitud_apertura` (
  `solicitud_apertura_id` int(100) NOT NULL,
  `curso_id` int(100) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `celular` int(10) DEFAULT NULL,
  `correo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `lunes` int(1) UNSIGNED ZEROFILL DEFAULT NULL,
  `martes` int(1) UNSIGNED ZEROFILL DEFAULT NULL,
  `miercoles` int(1) UNSIGNED ZEROFILL DEFAULT NULL,
  `jueves` int(1) UNSIGNED ZEROFILL DEFAULT NULL,
  `viernes` int(1) UNSIGNED ZEROFILL DEFAULT NULL,
  `sabado` int(1) UNSIGNED ZEROFILL DEFAULT NULL,
  `hora_tentativa` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `mensaje` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha_server` datetime(6) DEFAULT NULL,
  `domingo` int(1) UNSIGNED ZEROFILL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=DYNAMIC;

-- Indices de la tabla `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`log_id`) USING BTREE;

-- Indices de la tabla `web_cursos`
--
ALTER TABLE `web_cursos`
  ADD PRIMARY KEY (`curso_id`);

--
-- Indices de la tabla `web_cursos_contenido`
--
ALTER TABLE `web_cursos_contenido`
  ADD PRIMARY KEY (`curso_contenido_id`) USING BTREE;

--
-- Indices de la tabla `web_cursos_contenido_det`
--
ALTER TABLE `web_cursos_contenido_det`
  ADD PRIMARY KEY (`curso_contenido_det_id`) USING BTREE;

--
-- Indices de la tabla `web_cursos_icono`
--
ALTER TABLE `web_cursos_icono`
  ADD PRIMARY KEY (`curso_icono_id`) USING BTREE;

--
-- Indices de la tabla `web_cursos_icono_det`
--
ALTER TABLE `web_cursos_icono_det`
  ADD PRIMARY KEY (`curso_icono_det_id`) USING BTREE;

--
-- Indices de la tabla `web_cursos_modulo`
--
ALTER TABLE `web_cursos_modulo`
  ADD PRIMARY KEY (`curso_modulo_id`) USING BTREE;

--
-- Indices de la tabla `web_cursos_modulo_det`
--
ALTER TABLE `web_cursos_modulo_det`
  ADD PRIMARY KEY (`curso_modulo_det_id`) USING BTREE;

--
-- Indices de la tabla `web_eventos`
--
ALTER TABLE `web_eventos`
  ADD PRIMARY KEY (`evento_id`);

--
-- Indices de la tabla `web_organizacion`
--
ALTER TABLE `web_organizacion`
  ADD PRIMARY KEY (`id_organizacion`);

--
-- Indices de la tabla `web_pagos`
--
ALTER TABLE `web_pagos`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `web_servicios`
--
ALTER TABLE `web_servicios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `web_solicitud_apertura`
--
ALTER TABLE `web_solicitud_apertura`
  ADD PRIMARY KEY (`solicitud_apertura_id`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
ALTER TABLE `log`
  MODIFY `log_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
ALTER TABLE `web_cursos`
  MODIFY `curso_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id del registro', AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT de la tabla `web_cursos_contenido`
--
ALTER TABLE `web_cursos_contenido`
  MODIFY `curso_contenido_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT de la tabla `web_cursos_contenido_det`
--
ALTER TABLE `web_cursos_contenido_det`
  MODIFY `curso_contenido_det_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `web_cursos_icono`
--
ALTER TABLE `web_cursos_icono`
  MODIFY `curso_icono_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT de la tabla `web_cursos_icono_det`
--
ALTER TABLE `web_cursos_icono_det`
  MODIFY `curso_icono_det_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT de la tabla `web_cursos_modulo`
--
ALTER TABLE `web_cursos_modulo`
  MODIFY `curso_modulo_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT de la tabla `web_cursos_modulo_det`
--
ALTER TABLE `web_cursos_modulo_det`
  MODIFY `curso_modulo_det_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `web_eventos`
--
ALTER TABLE `web_eventos`
  MODIFY `evento_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `web_organizacion`
--
ALTER TABLE `web_organizacion`
  MODIFY `id_organizacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT de la tabla `web_pagos`
--
ALTER TABLE `web_pagos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT de la tabla `web_servicios`
--
ALTER TABLE `web_servicios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `web_solicitud_apertura`
--
ALTER TABLE `web_solicitud_apertura`
  MODIFY `solicitud_apertura_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- Restricciones para tablas volcadas
--