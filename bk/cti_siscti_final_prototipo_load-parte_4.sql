CREATE TABLE `sedes` 
( `id` INT NOT NULL AUTO_INCREMENT , 
  `nombre` VARCHAR(150) NOT NULL , 
  `descripcion` TEXT NOT NULL , 
  `estado` TINYINT(4) NOT NULL DEFAULT '1' , 
  PRIMARY KEY (`id`)
) ENGINE = InnoDB;


ALTER TABLE `aperturas` ADD `sede_id` INT NOT NULL DEFAULT 1 AFTER `grupo_id`;

INSERT INTO `sedes` (`id`, `nombre`, `descripcion`, `estado`) 
VALUES (NULL, 'SEDE TARAPOTO', 'Ciudad Universitaria', '1'), 
(NULL, 'SEDE MOYOBAMBA', '', '1'),
(NULL, 'SEDE RIOJA', '', '1');
